const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const { baseCss } = require('./webpack.ayudas');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserJSPlugin = require("terser-webpack-plugin");
const path = require('path');


module.exports = merge(common, {
  mode: 'production',
  target: ['es5', 'browserslist'],
  output: {
    publicPath: './',
    filename: '[name].[fullhash].js',
    chunkFilename: '[name].[fullhash].js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [MiniCssExtractPlugin.loader, ...baseCss],
      },
    ],
  },
  optimization: {
    minimizer: [new TerserJSPlugin({})]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css"
    }
  )],
});