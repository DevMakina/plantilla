import React from "react";
import Page1 from "./Page1";
import Page2 from "./Page2";
import Page3 from "./Page3";
import Page4 from "./Page4";
import Page5 from "./Page5";

export default {
  page1: ()=><Page1/>,
  page2: ()=><Page2/>,
  page3: ()=><Page3/>,
  page4: ()=><Page4/>,
  page5: ()=><Page5/>
};
