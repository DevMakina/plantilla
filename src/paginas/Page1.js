import React from "react";
import Page from "../components/Page";

const Page1 = () => {
  const textos = {
    es: () => {
      return (
        <>
          <h1> Titulos</h1>
        </>
      );
    },
    en: () => {},
  };

  return <Page textos={textos}></Page>;
};

export default Page1;
