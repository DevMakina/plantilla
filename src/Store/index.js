//https://medium.com/swlh/demystifying-the-folder-structure-of-a-react-app-c60b29d90836
import {useReducer} from 'react';
import { createContainer } from 'react-tracked';
import metadata from '../metadata';
const initialState = {
  language: metadata.language,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'MODIFY_STATE':
      return {
        ...state,
        [action.property]: action.value,
      };
    default:
      return state;
  }
};

const useValue = () => useReducer(reducer, initialState);

export const {
  Provider,
  useTrackedState,
  useUpdate: useDispatch,
} = createContainer(useValue);