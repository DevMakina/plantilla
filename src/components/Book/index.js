import "gsap";
import React, { useState, useEffect } from "react";
import MenuThumbs from "./MenuThumbs";
import { useParams } from 'react-router';
import { useHistory } from 'react-router';
import metadata from "../../metadata";
import paginas from "../../paginas";
import Creditos from "./Creditos";
import { Route, Switch } from "react-router-dom/cjs/react-router-dom.min";

const Book = (props) => { 
  //Estados del componente
  const [zoom, setZoom] = useState(1);
  const [marginLeft, setMarginLeft] = useState(0);
  const params = useParams();
  const { page } = params;  
  const history= useHistory();
  const {language, bookWidth, bookHeight, title} = metadata;
  const pagina = paginas[page] ?? paginas[`page${page}`]  

  const changeZoom = () => {
    let currentZoom = bookZoom(bookHeight, bookWidth);
    let currentMargin = document.body.clientWidth - bookWidth * currentZoom;
    let marginLeft = currentMargin / 2 > 0 ? currentMargin / 2 : 0;
    setZoom(currentZoom);
    setMarginLeft(marginLeft);    
  };

  //Zoom del libro
  const bookZoom = (bookHeight, bookwidth) => {
    let height = document.body.clientHeight,
      width = document.body.clientWidth,
      documentHeight = bookHeight,
      documentWidth = bookwidth,
      zoom;
    height <= width
      ? (zoom = height / documentHeight)
      : (zoom = width / documentWidth);
    let currentWidth = documentWidth * zoom;
    if (currentWidth >= width) {
      zoom = width / documentWidth;
    }
    return zoom;
  };

  
  const preventBehavior = (e) => {
    e.preventDefault(); 
  };

 
  useEffect(() => {
    history.push('/1')
    changeZoom();

    document.title = title[language];
    document.documentElement.setAttribute("lang", language);

    window.addEventListener("resize", changeZoom);
    document.body.addEventListener("resize", changeZoom);
    
    document.getElementById("root").addEventListener("touchmove", preventBehavior, {passive: false});

    return () => {
      window.removeEventListener('resize', changeZoom)
      document.body.removeEventListener("resize", changeZoom);
      document.getElementById("root").removeEventListener("touchmove", preventBehavior, {passive: false});
    }
  }, []);
  
  return (   
      <div                                                           
        className="book-container"
        style={{
          width: `${bookWidth}px`,
          height: `${bookHeight}px`,
          transform: `scale(${zoom})`,
          transformOrigin: "0px",
          marginLeft: marginLeft,
        }}
      > 
      <Switch> 
        <Route exact path="/creditos">
          <Creditos/>
        </Route>
        <Route exact path="/:page">
          <>
            {pagina && <>{pagina()}</>}
            <MenuThumbs/>          
          </>
        </Route>
      </Switch>         
    </div>
  );
}

export default Book;