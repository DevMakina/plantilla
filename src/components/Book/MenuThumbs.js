import React, { useState, useRef, useEffect } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Keyboard } from "swiper";
import "../../../node_modules/swiper/swiper.scss";
import { Link } from "react-router-dom";
import metadata from "../../metadata";
import { useDispatch, useTrackedState } from "../../store";
import {
  useLocation,
  useParams,
} from "react-router-dom/cjs/react-router-dom.min";
import { useFuncionesSonido } from "../Sound";
SwiperCore.use([Navigation, Keyboard]);

const MenuThumbs = (props) => {
  const dispatch = useDispatch();
  const state = useTrackedState();
  const { language } = state;
  const params = useParams();
  const location = useLocation();
  const { page } = params;
  const [showMenu, setShowMenu] = useState(false);

  let { Sounds } = useFuncionesSonido();

  const elementos = [
    {
      nombre: "background-off",
      ancho: 23,
      alto: 26,
      x: 116,
      y: 145,
      posiciones: [
        {
          x: 238,
          y: 26,
        },
      ],
    },
    {
      nombre: "background-on",
      ancho: 32,
      alto: 26,
      x: 139,
      y: 145,
      posiciones: [
        {
          x: 238,
          y: 26,
        },
      ],
    },
    {
      nombre: "menu",
      ancho: 95,
      alto: 84,
      x: 0,
      y: 110,
      posiciones: [
        {
          x: 0,
          y: 0,
        },
      ],
    },
    {
      nombre: "menu-siguiente",
      ancho: 45,
      alto: 58,
      x: 95,
      y: 145,
      posiciones: [
        {
          x: 395,
          y: 409,
        },
      ],
    },
    {
      nombre: "menu-anterior",
      ancho: 20,
      alto: 29,
      x: 161,
      y: 110,
      posiciones: [
        {
          x: 419,
          y: 33,
        },
      ],
    },
    {
      nombre: "narration-on",
      ancho: 33,
      alto: 35,
      x: 132,
      y: 110,
      posiciones: [
        {
          x: 228,
          y: 60,
        },
      ],
    },
    {
      nombre: "narration-off",
      ancho: 33,
      alto: 35,
      x: 99,
      y: 110,
      posiciones: [
        {
          x: 228,
          y: 60,
        },
      ],
    },
  ];
  const botonesSonido = ["narration"];
  const [soundsState, setSoundsState] = useState({
    narration: true,
    background: true,
    interaccion: true,
  });
  const [topPosition, setTopPosition] = useState(0);

  const menuElement = useRef();
  const { totalPages, languages } = metadata;
  const elementoMenu = elementos.find((item) => item.nombre === "menu");

  useEffect(() => {
    setShowMenu(false);
    setTopPosition(`-${menuElement?.current?.offsetHeight}`);
  }, [page, location.pathname]);

  return (
    <>
      <div
        className="menu-thumbs"
        ref={menuElement}
        style={{ top: `${showMenu ? "0" : topPosition}px` }}
      >
        {botonesSonido.map((type) => {
          const btnState = soundsState[type];
          const elemento = elementos.find(
            (item) => item.nombre === `${type}-${btnState ? "on" : "off"}`
          );
          const { ancho, alto, posiciones, x, y } = elemento ?? {};
          return (
            <button
              key={`button-${type}`}
              className={`btn button-${type}`}
              style={{
                width: ancho + "px",
                height: alto + "px",
                left: posiciones?.[0].x + "px",
                top: posiciones?.[0].y + "px",
                backgroundImage:
                  "url('" + require(`../../img/botones.png`).default + "')",
                backgroundPosition: "-" + x + "px -" + y + "px",
              }}
              onClick={() => {
                Sounds[type].state = !btnState;
                Sounds[type].sound.mute(btnState);
                if (btnState) {
                  Sounds[type].sound.pause();
                } else {
                  Sounds[type].sound.play();
                }
                setSoundsState({ ...soundsState, [type]: !btnState });
              }}
            ></button>
          );
        })}

        {languages?.length > 1 && (
          <>
            {languages.map((lang) => (
              <button
                className={`language language${i}`}
                onClick={() => {
                  dispatch({
                    type: "MODIFY_STATE",
                    property: "language",
                    value: lang,
                  });
                }}
                key={`language-${lang}`}
              >
                {lang}
              </button>
            ))}
          </>
        )}

        <div className="contenedor-swiper">
          <Swiper
            slidesPerView={3}
            spaceBetween={20}
            keyboard={true}
            navigation
          >
            {Array(totalPages)
              .fill()
              .map((item, index) => {
                //AE: Las páginas empiezan en 1;
                let numeroPagina = index + 1;
                let image = require(`../../img/thumbs/${language}/thumb0${numeroPagina}.png`).default;
                if (image) {
                  return (
                    <SwiperSlide key={`thumb${numeroPagina}`}>
                      <Link
                        to={`/${numeroPagina}`}
                        style={{
                          backgroundImage: `url(${image})`,
                          height: `${metadata.bookHeight / 5}px`,
                          width: "100%",
                        }}
                      />
                    </SwiperSlide>
                  );
                }
              })}
          </Swiper>
        </div>
      </div>
      {parseInt(page) > 1 && (
        <button
          className="btn btn-menu"
          style={{
            width: elementoMenu?.ancho + "px",
            height: elementoMenu?.alto + "px",
            left: elementoMenu?.posiciones[0].x + "px",
            top:
              `${
                elementoMenu?.posiciones[0].y +
                (showMenu
                  ? menuElement?.current?.offsetHeight
                  : 0)
              }` + "px",
            backgroundImage: "url('" + require(`../../img/botones.png`).default + "')",
            backgroundPosition:
              "-" + elementoMenu?.x + "px -" + elementoMenu?.y + "px",
          }}
          onClick={() => {
            setShowMenu(!showMenu);
          }}
        >
          Menú
        </button>
      )}
    </>
  );
};

export default MenuThumbs;
