const { useParams, useHistory } = require("react-router-dom/cjs/react-router-dom");
import { Link } from "react-router-dom/cjs/react-router-dom";
import metadata from "../../metadata";
import React, { useEffect } from "react";


const Botones = (props) => {
  const params = useParams();
  const history = useHistory();
  const { page } = params; 

  const elementosBotones  = [
    {
      "nombre": "siguiente",
      "ancho": 87,
      "alto": 110,
      "x": 0,
      "y": 0,
      "posiciones": [
        {
          "x": 937,
          "y": 646
        }
      ]
    },
    {
      "nombre": "anterior",
      "ancho": 87,
      "alto": 110,
      "x": 87,
      "y": 0,
      "posiciones": [
        {
          "x": 0,
          "y": 646
        }
      ]
    }
  ]

  const links = {
    'anterior': `${parseInt(page) -1}`,
    'siguiente': `${parseInt(page) + 1}`,
    /* "leer": '2',
    'maquina': 'creditos' */
  }
  
  useEffect(() => {
    window.addEventListener("keydown", checkKey);
    return () => {
      window.removeEventListener("keydown", checkKey);
    };
  }, [page]);

  //funcionalidad con el teclado
  const checkKey = (e) => {
    e = e || window.event;
    if (e.keyCode == "37") {
      // left arrow
      if(page > 1){
        history.push(`/${parseInt(page) - 1}`);
      }
    } else if (e.keyCode == "39") {
      if(page < metadata.totalPages){
        // right arrow
        history.push(`/${parseInt(page) + 1}`);
      }
    }
  };

  const mostrarBotones = () => {
    let botones = [];
    switch(parseInt(page)){
      case 1:
        botones = ['siguiente' ];
        break;
      case metadata.totalPages:
        botones = ['anterior'];
        break;
      default:
        botones = ['siguiente', 'anterior']
        break;
    }
    
    return botones;
  }

  const crearBotones = () => {
    let casosBotones = mostrarBotones();
    return elementosBotones.filter(boton => casosBotones.includes(boton.nombre))
  }

  const botones = crearBotones();
  const {funcionSiguiente, pageElements={}} = props;

  return (
    <>
      {
        botones.map((elemento) => {
          const {nombre, alto, ancho, posiciones, x, y, funcionBoton} = elemento;
          const link = links?.[nombre]
          const styles = {
            width: ancho + "px",
            height: alto + "px",
            left: posiciones[0].x + "px",
            top: posiciones[0].y + "px",
            backgroundImage: "url('" + require(`../../img/botones.png`).default + "')",
            backgroundPosition: "-" + x + "px -" + y + "px"
          }
          return (
            <React.Fragment key={`btn-${nombre}`}>
              {
                link  ?
                <Link ref={a => (pageElements[`btn-${nombre}`] = a)} to={`/${link}`} className="btn" id={`btn-${nombre}`} style={styles}
                  onClick={(e)=>{
                    if(nombre === 'siguiente' && funcionSiguiente){
                      e.preventDefault();
                      funcionSiguiente();
                    }
                  }}
                >{nombre}</Link>
                :
                <button ref={button => (pageElements[`btn-${nombre}`] = button)} className="btn" id={`btn-${nombre}`} style={styles} onClick={funcionBoton ? ()=>{funcionBoton()} : null}>{nombre}</button>
              }
            </React.Fragment>
          )                
        })
      }
    </>
  )
}

export default Botones;