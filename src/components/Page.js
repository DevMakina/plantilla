import React, { useEffect } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { useFuncionesSonido } from "./Sound";
import Botones from "./Book/Botones";
import { useTrackedState } from "../store";

const Page = (props) => {
  //language global property
  const state = useTrackedState();
  const { language } = state;

  const {
    children,
    textos,
    elementos,
    sonidoPrincipal,
    funcionSiguiente,
    animaciones,
  } = props;
  const params = useParams();
  const { page } = params;
  const pageElements = {};
  const texto = textos?.[language];

  const { playSound, pauseSound } = useFuncionesSonido();

  useEffect(() => {
    pauseSound({ type: "narration" });
    if (sonidoPrincipal) {
      playSound({ sound: sonidoPrincipal, type: "narration" });
    }
    if (animaciones?.principal) {
      animaciones.principal(pageElements);
    }
  }, [animaciones]);

  return (
    <>
      <Botones
        funcionSiguiente={funcionSiguiente}
        pageElements={pageElements}
      />
      <div className={`page page0${page}`}>
        <article ref={(article) => (pageElements.content = article)}>
          {elementos && (
            <>
              {elementos.map((elemento, index) => {
                const { nombre, alto, ancho, posiciones, x, y } = elemento;
                const interaccion = animaciones?.[nombre];
                return (
                  <div
                    ref={(div) => (pageElements[nombre] = div)}
                    key={`page-${page}-${nombre}`}
                    className="element"
                    onClick={
                      interaccion
                        ? (e) => {
                            interaccion(pageElements, e);
                          }
                        : null
                    }
                    data-name={nombre}
                    id={`page-${page}-${nombre}`}
                    style={{
                      width: ancho + "px",
                      height: alto + "px",
                      left: posiciones[0].x + "px",
                      top: posiciones[0].y + "px",
                      backgroundImage:
                        "url('" + require(`../img/page${page}.png`).default + "')",
                      backgroundPosition: "-" + x + "px -" + y + "px",
                      cursor: interaccion ? "pointer" : "auto",
                    }}
                  />
                );
              })}
            </>
          )}
          {texto && (
            <div
              ref={(div) => (pageElements.texts = div)}
              className={`texts texts_${language}`}
            >
              {typeof texto === "function" ? texto() : texto}
            </div>
          )}
        </article>
        {children}
      </div>
    </>
  );
};

export default Page;
