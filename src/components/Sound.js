import { useTrackedState } from "../store";
import { Howl} from "howler";

let Sounds = {
  interaction: {
    sound: '',
    state: true
  },
  narration: {
    sound: '',
    state: true
  },
  background: {
    sound: '',
    state: true
  }
}

const useFuncionesSonido = () => {
  const state = useTrackedState(); 
  const {language: stateLanguage} = state;

  const createSound = ({sound, type, volume = 1, loop, language=stateLanguage}) => {
    if (sound !== "") {
        let audioUrl;
        try {
          type === "narration"
              ? (audioUrl = require(`../sounds/${type}/${language}/${sound}.mp3`).default)
              : (audioUrl = require(`../sounds/${type}/${sound}.mp3`).default);

          Sounds[type].sound = new Howl({
            src: [audioUrl],
            loop: loop || false,
            volume,
            html5: true,
            _html5: true,
          });

          if (Sounds[type].state) {
            Sounds[type].sound.play();
          }
        } catch (error) {
          return error;
        }
    }
  };

  const playSound = ({sound, type, volume, loop, language}) => {
    if (sound !== "") {
      pauseSound({type});
      createSound({sound, type, volume, loop, language});
    }
  };
  const pauseSound = ({type}) => {
    if (type && Sounds[type].sound !== "") {
      Sounds[type].sound.unload();
    }
  };
  
  return {Sounds, playSound, pauseSound}
}

export {useFuncionesSonido};