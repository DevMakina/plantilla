var contador = 0;
import imagesPerLoad from "./imagesPerLoad";
var imagesLoaded = [];

function cargaAjax(value, length) {
  var request = new XMLHttpRequest();
  request.open("GET", "./src/img/" + value, true);
  request.onload = function () {
    if (request.status >= 200 && request.status < 400) {
      // Success!
      var img = document.createElement("img");
      img.setAttribute("src", "./src/img/" + value);
      imagesLoaded.push(img);
      contador += 1;
      if (contador === length) {
        document.getElementById("percentage").innerText = "100%";
        var elem = document.getElementById("preload");
        elem.classList.add("fadeIn");
        setTimeout(() => {
          elem.parentNode.removeChild(elem);
        }, 4000);
      }
    } else {
      // We reached our target server, but it returned an error
    }
  };
  request.onerror = function () {
    console.error(request.error);
    // There was a connection error of some sort
  };

  request.onprogress = function () {
    // There was a connection error of some sort
    var number = Math.round((contador * 100) / length);
    document.getElementById("percentage").innerText = number + "%";
  };
  request.send();
}

function cargar(url, length) {
  var img = new Image();
  img.onload = function () {
    contador++;
    imagesLoaded.push(img);
    var number = Math.round((contador * 100) / length);
    document.getElementById("percentage").innerText = number + "%";
    if (contador === length) {
      document.getElementById("percentage").innerText = "100%";
      var elem = document.getElementById("preload");
      elem.classList.add("fadeIn");
      setTimeout(() => {
        elem.parentNode.removeChild(elem);
      }, 2000);
    }
  };
  img.src = require("../img/" + url).default;
}

function isCached(url) {
  var img = new Image();
  img.src = "./src/img/" + url;
  return img.complete || img.width + img.height > 0;
}

function preloadImages() {
  for (var i = 0; i < imagesPerLoad.length; i++) {
    cargar(imagesPerLoad[i], imagesPerLoad.length);
  }
}

preloadImages();
