import React from "react";
import { Route, Switch} from "react-router";
import Book from "./components/Book/";
//Preload
import "./modules/preloadImages";



const App = () => {
  return (    
    <Switch>
      <Route exact path="/">
        <Book/>
      </Route> 
      <Route exact path="/:page">
        <Book/>
      </Route>  
    </Switch>
  );
};

export default App;
