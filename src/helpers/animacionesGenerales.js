import { TweenMax, TimelineMax } from "gsap/all"

/***********************************************
                V E H I C U L O S                
************************************************/
//Movimiento de los carros
export function movimientoCarro(el, duracion, pInicial, pFinal, delay) {
    let animMovimientoCarros = new TimelineMax({ repeat: -1, repeatDelay: delay })
    animMovimientoCarros.fromTo(el, duracion, { x: pInicial, ease: Linear.easeNone }, { x: pFinal, ease: Linear.easeNone })
    return animMovimientoCarros;
}

//Rotación de las ruedas
export function ruedas(pag, el, numRuedas, rotacion, duracion) {
    for (let i = 1; i <= numRuedas; i++) {
        let animRuedasCarro = new TimelineMax({ repeat: -1 });
        animRuedasCarro.to(pag[`${el}${i}`], duracion, {
            rotation: rotacion,
            ease: Linear.easeNone,
        });
    }
}

export function nombreParteBoca(elCaja, elNombre) {
  TweenMax.to(elCaja, 1, { rotationY: -180 })
  TweenMax.to(elNombre, 1, { rotationY: 0 })
}

//Parpados
export function parpadeo(parpado, delay) {
    let parpadeo = new TimelineMax({
        paused: true,
        repeat: 1,
        yoyo: true,
        onCompleteParams: ["{self}"],
        onComplete: function () {
            TweenLite.delayedCall(10 * Math.random() + 0.1, parpadeo.restart, [], parpadeo);
        }
    });

    let lid_top = TweenMax.from(parpado, 0.2, {
        opacity: 0,
        ease: SteppedEase.config(1)
    });

    parpadeo.add([lid_top]);
    parpadeo.progress(1).progress(0);
    parpadeo.delay(delay);
    parpadeo.play();

    return parpadeo;
}

//Oscilacion en y
export function oscilacion(el, duracion, oscilacion, repeat, yoyo, efecto) {
    let animElemento = new TimelineMax({ repeat: repeat, yoyo: yoyo});
    animElemento.to(el, duracion, {
        y: oscilacion,
        ease: efecto,
    });
    return animElemento;
}

//Oscilacion en x 
export function oscilacionX(el, duracion, posicionInicial, nuevaPosicion, repeat, yoyo, efecto) {
    TweenMax.set(el,{x: posicionInicial})    

    let animElemento = new TimelineMax({ repeat: repeat, yoyo: yoyo });
    animElemento.to(el, duracion, {
        x: nuevaPosicion,
        ease: efecto,
    });
    return animElemento;
}

//rotation
export function rotacion(el, duracion, origen, rotacion, repetir, yoyo, efecto) {
    let rotacionObj = new TimelineMax({ repeat: repetir, yoyo: yoyo });
    rotacionObj.to(el, duracion, {
        transformOrigin: origen,
        rotation: rotacion,
        ease: efecto,
    });

    return rotacionObj;
}


//Rand
export function rand(max, min) {
    return Math.random() * (max - min) + min;
}

//Arboles
export function movimientoArboles(pag, el, numEl, rotation, time) {
    for (let i = 1; i <= numEl + 1; i++) {
        TweenMax.set([pag[`${el}${i}`]], {
            transformOrigin: "bottom center",
        });

        let a, sim;
        a = i % 2;

        if (a == 0) {
            sim = -rotation;
        } else {
            sim = rotation;
        }

        let elemento = new TimelineMax({
            repeat: -1,
            yoyo: true,
        });
        elemento.fromTo(
            [pag[`${el}${i}`]],
            time,
            {
                rotation: sim,
                ease: Power1.easeInOut,
            },
            {
                rotation: -sim,
                ease: Power1.easeInOut,
            }
        );
    }
    return elemento;
}

//Desplazamiento X
export function desplazamientoX(el, duracion, xInicial, xFinal, ease) {

    TweenMax.set(el, { x: xInicial })

    let animDesplazamientoX = new TimelineMax()
    animDesplazamientoX.to(el, duracion, { x: xFinal, ease: ease })

    return animDesplazamientoX;
}
export function desplazamientoXRepeat(el, duracion, xInicial, xFinal, ease, repeat) {

    TweenMax.set(el, { x: xInicial })

    let animDesplazamientoXRepeat = new TimelineMax()
    animDesplazamientoXRepeat.to(el, duracion, { x: xFinal, ease: ease,repeat: repeat   })

    return animDesplazamientoXRepeat;
}



//Textos
export function text(pag, el, numEl, delay) {
    for (let i = 1; i <= numEl; i++) {
        TweenMax.set(pag[`${el}${i}`], { opacity: 0 })
    }

    for (let j = 1; j <= numEl; j++) {
        let animTexto = new TimelineMax({ delay: delay[`texto${j}`] })
        animTexto.to(pag[`${el}${j}`], 0.5, { opacity: 1 })
    }

}

//Senal Paso
export function senalPaso(luzVerde, luzRoja) {
    TweenMax.set([luzVerde, luzRoja], { opacity: 0.5 })

    TweenMax.to(luzVerde, 0.5, { opacity: 1, repeat: -1, yoyo: true, ease: Linear.easeNone })
    TweenMax.to(luzRoja, 0.5, { opacity: 1, delay: 0.5, repeat: -1, yoyo: true, ease: Linear.easeNone })
}

//Puntos
export function puntos(pag, el, num) {
    for (let i = 1; i <= num; i++) {
        TweenMax.to(pag[`${el}${i}`], rand(1, 0.7), { opacity: 0.5, repeat: -1, yoyo: true, ease: Power1.easeInOut })
    }
}