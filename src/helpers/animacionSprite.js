import { TweenMax, TimelineMax } from "gsap";

function animacionSprite(elemento, imagePositions, defaultWidth, defaultHeight, defaultTop, defaultLeft, repeat) {
  const animationSequence = new TimelineMax({ repeat: repeat });
  const positionDuration = 0;
  const imageDelay = 0.15;
  for (let i = 0; i < imagePositions.length; i++) {
    const position = imagePositions[i];
    animationSequence.add(
      animateToPosition(elemento, position, positionDuration, defaultWidth, defaultHeight, defaultTop, defaultLeft),
      i * (positionDuration + imageDelay)
    );
  }
}

function animateToPosition(element, position, duration, defaultWidth, defaultHeight, defaultTop, defaultLeft) {
  const xPosition = position.x + "px";
  const yPosition = position.y + "px";
  const width = position?.width ?? defaultWidth + 'px';
  const height = position?.heigth ?? defaultHeight + 'px';
  const top = position.top ?? defaultTop + 'px';
  const left = position.left ?? defaultLeft + 'px';
  
  return TweenMax.to(element, duration, {
    backgroundPosition: `${xPosition} ${yPosition}`,
    width,
    height,
    top,
    left
  });
}

/*
function nino_idle_1(pag) {
  const elemento = pag.nino_idle_1;
  const imagePositions = [
    { x: -967, y: -2121 },
    { x: -484, y: -2121 },
    { x: -0, y: -2121 },
    { x: -1530, y: -1696 },
  ];
  animacionSprite({elemento, imagePositions, defaultWidth:483})
const animaciones = {
  principal: (pag) => {      
    nino_idle_1(pag);
  },
  nino_idle_1: (pag) => {
    alert('hola')
  }
};
  
*/

export default animacionSprite;